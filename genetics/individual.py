import random
from typing import List

from genetics import gene


class Individual:
    genome: List[gene.Gene]

    def __init__(self, genome_length=10):
        self.genome = []
        for i in range(genome_length):
            self.genome.append(gene.Gene())

    def mutate(self):

        choice = random.randrange(3)

        if choice == 0 and len(self.genome) > 0:
            # remove gene
            index = random.randrange(len(self.genome))
            del self.genome[index]

        elif choice == 1:
            # mutate existing genes
            for g in self.genome:
                g.maybeMutate()

        elif choice == 2:
            # add gene
            if random.randint(0, 1):
                # TODO: Add at random place in genome
                self.genome.append(gene.Gene())

        # combinator style
        return self