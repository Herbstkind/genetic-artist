import random

class Gene:
    """Represents one element of a genome (not the whole genome itself!)"""

    def __init__(self):
        self.size_f = random.random()
        self.position_f = (random.random(),  # fraction of display width
                           random.random()) # fraction of display height
        self.r = random.randint(0, 255)
        self.g = random.randint(0, 255)
        self.b = random.randint(0, 255)

    def color(self):
        return self.r, self.g, self.b

    def maybeMutate(self):
        if random.randint(0,1):
            self.size_f = self.maybeMutateFactor(self.size_f)
            self.position_f = (self.maybeMutateFactor(self.position_f[0]),
                               self.maybeMutateFactor(self.position_f[1]))
            self.g = self.maybeMutateFactor(self.g / 255) * 255
            self.r = self.maybeMutateFactor(self.r / 255) * 255
            self.b = self.maybeMutateFactor(self.b / 255) * 255

    def maybeMutateFactor(self, factor):
        """Mutate a factor that lies between 0 and 1.

        :type factor: int
        """
        assert factor >= 0
        assert factor <= 1
        if random.randint(0, 1):
            move_towards = random.randint(0,1)
            return factor + (move_towards - factor) * random.random()
        return factor
