#!/usr/bin/env python3
import copy

import pygame

from display import pane, screen
from genetics import individual


def select_individual():
    while True:
        events = pygame.event.get()
        for e in events:
            if e.type == pygame.QUIT:
                pygame.quit()
                quit()
            elif e.type == pygame.KEYDOWN:
                # numbers are in the corners of the number block
                if e.key == pygame.K_7:
                    return 0
                if e.key == pygame.K_1:
                    return 1
                if e.key == pygame.K_9:
                    return 2
                if e.key == pygame.K_3:
                    return 3


s = screen.Screen()
dim = (s.dimensions[0] // 2,
       s.dimensions[1] // 2)

grid = [
    (pane.Pane(s, dim, offset=(0 * dim[0], 0 * dim[1])), individual.Individual()),
    (pane.Pane(s, dim, offset=(0 * dim[0], 1 * dim[1])), individual.Individual()),
    (pane.Pane(s, dim, offset=(1 * dim[0], 0 * dim[1])), individual.Individual()),
    (pane.Pane(s, dim, offset=(1 * dim[0], 1 * dim[1])), individual.Individual()),
]

def render():
    s.clear()
    for pane_with_individual in grid:
        pane_with_individual[0].drawIndividual(pane_with_individual[1])
    pygame.display.update()

while True:
    render()
    winner_i = select_individual()
    for i0 in range(len(grid)):
        if i0 != winner_i:
            grid[i0][1].genome = copy.deepcopy(grid[winner_i][1].genome)
            # don't mutate the winner. User might want to wait for better mutations before selecting one.
            grid[i0][1].mutate()
