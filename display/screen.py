import pygame

class Screen:
    def __init__(self, dimensions=(800,600)):
        # initialize pygame
        pygame.init()
        self._game_display = pygame.display.set_mode(dimensions)
        self._sf = pygame.Surface(dimensions)
        # initialize parameters
        self.dimensions = dimensions

    def pixel(self, position, color):
        self._game_display.set_at(position, color)

    def drawElement(self, position, size, color):
        "Abstracts over the visual representation of a gene (circle, square, ...)"
        pygame.draw.circle(self._game_display, color, position, size)

    def clear(self):
        self._game_display.fill((0, 0, 0))
