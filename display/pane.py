from typing import Tuple

from genetics import gene


class Pane:
    "Represents a section of a Screen"

    dimensions: Tuple[int, int]
    offset: Tuple[int, int]

    def __init__(self, screen, dimensions, offset=(0, 0)):
        assert min(offset) >= 0
        assert offset[0] < screen.dimensions[0]
        assert offset[1] < screen.dimensions[1]
        self.screen = screen
        self.dimensions = dimensions
        self.offset = offset
        max_diameter = 100
        self.max_radius = max_diameter // 2

    def allelePosition(self, allele, max_radius):
        position_range = (self.dimensions[0] - 2 * max_radius,
                          self.dimensions[1] - 2 * max_radius)
        return (int(max_radius + allele.position_f[0] * position_range[0]),
                int(max_radius + allele.position_f[1] * position_range[1]))

    def drawIndividual(self, individual):
        # self.drawCircles(individual)
        self.drawVoronoi(individual)

    def drawCircles(self, individual):
        """
        Render the visual representation of an INDIVIDUAL.

        :type individual: genetics.individual.Individual
        """
        g: gene.Gene
        for g in individual.genome:
            # calculate position in pane
            relative_position = self.allelePosition(g, self.max_radius)

            # assert that position is in range
            assert min(relative_position) >= self.max_radius
            assert relative_position[0] < self.dimensions[0] - self.max_radius
            assert relative_position[1] < self.dimensions[1] - self.max_radius

            # draw to screen
            absolute_position = (relative_position[0] + self.offset[0],
                                 relative_position[1] + self.offset[1])
            self.screen.drawElement(absolute_position, int(g.size_f * self.max_radius), g.color())

    def drawVoronoi(self, individual):
        import pygame
        for x in range(self.dimensions[0]):
            for y in range(self.dimensions[1]):
                # find closest allele point
                color = None
                for g in individual.genome:
                    point = self.allelePosition(g, 0)
                    dist = abs(point[0]-x) + abs(point[1]-y)  # manhattan distance
                    if color is None or dist < min_dist:
                        min_dist, color = dist, g.color()
                self.screen.pixel((self.offset[0] + x, self.offset[1] + y), color)
        pygame.display.update()
